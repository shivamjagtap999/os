#!/bin/bash

#Write a program to print the table of a given number.

clear
echo "Enter num"
read num

for i in {1..10..1}
do
	res=`expr $num \* $i`
	echo "$res"
done
exit
