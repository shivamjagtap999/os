#!/bin/bash
#Write a program to find the factorial of given number.

clear
echo "Enter num"
read num
res=1
while [ $num -gt 1 ]
do
	res=`expr $num \* $res`
	num=`expr $num - 1`
done
echo "$res"
exit
