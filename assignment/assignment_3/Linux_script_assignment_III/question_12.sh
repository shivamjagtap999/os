#!/bin/bash

#Write a shell script to accept a filename as argument and displays the last modification
#time if the file exists and a suitable message if it doesn’t exist.

method -2
clear

echo "Enter filename : "
read fname

if [ -e $fname ]
then
	if [ -f $fname ]
	then
	echo "last modification time  : "
		stat --format="%y" $fname
	fi
else
	echo "$fname doesn't exist"
fi

exit

method -2

clear

if [ -e $1 ]
then
	if [ -f $1 ]
	then
	echo "last modification time  : "
		stat --format="%y" $1
	fi
else
	echo "$1 doesn't exist"
fi

exit