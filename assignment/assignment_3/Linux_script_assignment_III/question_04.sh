#!/bin/bash

#Write a shell script to determine whether a given number is prime or not

clear

echo "Enter number : "
read num

if [ $num -eq 1 ]
then
	echo "$num is composite"
	exit
fi

flag=1
i=2
cond=`echo "$num / 2" | bc`
#echo "$cond num/2"
while [ $i -le $cond ]
do
	if [ `expr $num % $i` -eq 0 ]
	then
		flag=0
		echo "$num is not prime"
		exit
	fi
	i=`expr $i + 1`
done

if [ $flag -eq 1 ]
then
	echo "$num is prime"
fi
exit