#!/bin/bash

#Accept the two file names from user and append the contents in reverse case of first file
#into second file.

clear

echo "Enter first filename"
read ffname

echo "Enter second filename"
read sfname

cat $ffname | rev | cat >> $sfname

echo "after concatenating $ffname and $sfname :"
cat $sfname

exit