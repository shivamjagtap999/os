#!/bin/bash

#Print the following pattern (traingle pattern)
#*   
#*  *   
#*  *  *   
#*  *  *  *   
#*  *  *  *  * 
clear

for i in {1..5}
do
	j=1
	while [ $j -le $i ]
	do
		echo -n "*  "
		j=`expr $j + 1`
	done
	echo " "
done

exit
