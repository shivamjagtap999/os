#!/bin/bash

#Write a shell script to display only executable files of current directory

clear
echo "all executable files of current directory : "
for i in $(ls .)
do
	if [ -f $i ]
	then
		if [ -x $i ]
		then
			echo "$i"
		fi
	fi
done

exit