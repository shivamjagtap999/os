#!/bin/bash

#Write a shell script to display only hidden file of current directory.

clear

echo "hidden file of current directory : "
ls -a | egrep '^\.'
exit