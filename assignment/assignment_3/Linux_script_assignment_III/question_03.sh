#!/bin/bash

#Write a shell script to accept the name from the user and check whether user entered
#name is file or directory. If name is file display its size and if it is directory display its
#contents.

clear

echo "Enter name : "
read pname

if [ -e $pname ]
then
	if [ -f $pname ]
	then
		echo "$pname is file "
		echo "size  : "
		stat --format="%B" $pname
	elif [ -d $pname ]
	then
		echo "$pname is directory"
		echo "contents : "
		ls -1
	else
		echo "$pname is neither file nor directory"
	fi
else
	echo "invalid name"
fi

exit