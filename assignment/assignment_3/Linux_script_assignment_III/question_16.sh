#!/bin/bash

#Write a shell script to display welcome message to the user along with contents of his
#home directory. Ensure that shell script will execute automatically when user login to the
#shell.

clear

echo "Welcome $USER"

echo "Home directory contents"
ls -1 /home/osboxes/

exit
