#!/bin/bash

#Write a program to calculate gross salary if the DA is 40%, HRA is 20% of basic salary.
#Accept basic salary form user and display gross salary (Result can be floating point
#value).

clear
echo "Enter basic salary : "
read bs

da=`echo "0.4 * $bs" | bc`
hra=`echo "0.2 * $bs" | bc`
res=`echo "$bs + $da + $hra" | bc`
echo "gross salary = $res"
exit
