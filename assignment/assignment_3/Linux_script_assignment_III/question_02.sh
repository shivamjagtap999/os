#!/bin/bash
#Write a shell script to display menu like “1. Date, 2. Cal, 3. Ls, 4. Pwd, 5. Exit” and
#execute the commands depending on user choice

function menu()
{
	echo "1.Date"
	echo "2.Cal"
	echo "3.Ls"
	echo "4.Pwd"
	echo "5.Exit"
	echo "Enter your choice"
}
clear 

menu
read choice

while [ $choice -ne 5 ]
do

case $choice in
	1)
		echo 'Date : '
		date +"%d/%m/%y"
		;;
	2)
		echo 'Cal : '
		cal 2021
		;;
	3)
		echo 'Ls : '
		ls -1
		;;
	4)
		echo 'Pwd : '
		pwd
		;;
	*)
		echo "Invalid choice entry"
		;;
esac

menu 
read choice

done

exit
