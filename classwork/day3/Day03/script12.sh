#!/bin/bash


#script to function to do addition of 2 number

function addition( )
{
	res=`expr $1 + $2`
		echo "$res"
}

clear

echo "script stared"

echo -n "enter n1:"
read n1

echo -n "enter n2:"
read n2

#syntax-1  echo used to printing purpose
#addition $n1 $n2

#syntax-2
sum=$(addition $n1 $n2)
echo "sum : $sum"
echo "script exited"

exit
