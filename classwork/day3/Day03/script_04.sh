
#!bin/bash

#script to calculate area of circle


clear

PI=3.14

echo -n "enter radius : "
read rad

area=`expr "$PI * $rad * $rad" | bc`
echo "area of circle is : $area"

exit

