#!/bin/bash

#script to check user entered is leap year or not

#if (yr %4 ==0 && yr% 100 !=0 || yr % 400 ==0)

clear

echo -n "enter year : "
read yr

if [ `expr $yr % 4` -eq 0 -a `expr $yr % 100` -ne 0 -o `expr $yr % 400` -eq 0 ]
then
	echo "$yr is a leap yr"
else 
	echo "$yr is  not a leap yr"
fi

exit
