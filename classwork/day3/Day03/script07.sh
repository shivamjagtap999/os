
#!/bin/bash

# print table of given no

clear

echo -n "enter number : "
read num 
#while loop
i=1
echo "while => table of $num is :"

while [ $i -le 10 ]
do 
	res=`expr $num \* $i`
	echo " $res "
	i=`expr $i + 1` # i++

done

#until loop
i=1
echo "until=>table of $num is :"

while [ $i -gt 10 ]
do 
	res=`expr $num \* $i`
	echo " $res "
	i=`expr $i + 1` # i++

done
#for loop

echo "for => table of $num is : "
for i in {1..10..2}
do
	res=`expr $num \* $i`
	echo "$res"

done
exit
